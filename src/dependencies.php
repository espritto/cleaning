<?php
// DIC configuration

$container = $app->getContainer();

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// Twig
$container['view'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    $view = new \Slim\Views\Twig($settings['template_path'], [
        //'cache' => 'path/to/cache'
    ]);
    return $view;
};

// MySQL
$container['db'] = function ($c) {
    //$settings = $c->get('settings')['db_local'];
    $settings = $c->get('settings')['db'];
    return new DBQuery($settings["db"], $settings["user"], $settings["pass"], $settings["host"]);
};

$container['tools'] = function ($c) {
    
    
    return [
        'calc_sum' => function($order){

            $lines = json_decode($order['pillows']);
            $prices = [
                [300,250,250,230],
                [330,280,280,260],
                [370,320,320,300]
            ];
            $total = 0;
            $min = 9999;
            
            foreach($lines as $line){
                $sum = $prices[$line->size - 1][$line->type - 1];
                if($sum < $min)
                    $min = $sum;
                $total += $sum;
            }

            if(count($lines) >= 3) {
                $total -= $min;
            }

            if($order['delivery']) {
                if($total < 1000) 
                    $total += 150;
            }
            
            return $total;
        }
    ];
    
};
