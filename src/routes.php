<?php
// Routes

$app->get('/manager', function ($request, $response, $args) {

    return $this->view->render($response, 'admin.html', [
        'page' => 'lalala'
    ]);

});

$app->get('/[{page}]', function ($request, $response, $args) {
    
    $page = $args['page'] ? $args['page'] : "home";

    if($page == 'home') {
        $title = 'Главная страница';
    } else if($page == 'about') {
        $title = 'О нас';
    } else if($page == 'faq') {
        $title = 'Частые вопросы';    
    } else if($page == 'info') {
        $title = 'Методы чистки подушек';
    } else if($page == 'order') {
        $title = 'Сделать заказ';
    } else if($page == 'prices') {
        $title = 'Цены на чистку';
    } else {
        $title = 'Страница не найдена';
        $page = '404';
    }


    return $this->view->render($response, 'main.html', [
        'page' => $page,
        'template' => 'page_' . $page . '.html',
        'show_banner' => $page == "home",
        'title' => $title
    ]);

});

$app->post('/ajax/order', function ($request, $response, $args) {

    $order = $request->getParsedBody();
    $db = $this->db;
    
    $client_id = $db->Query('clients')->where('phone', $order['phone'])->select_one('id');

    if($client_id === false){

        $client_id = $db->Query('clients')->insert([
            'phone' => $order['phone'],
            'fio' => $order['fio'],
            'orders_count' => 0,
            'total_cost' => 0,
            'register_date' => time()
        ]);

    }

    $db->Query('orders')->insert([
        'status' => 0,
        'date' => time(),
        'client_id' => $client_id,
        'delivery' => $order['isDelivery'] ? 1 : 0,
        'street' => $order['street'],
        'house' => $order['house'],
        'apartment' => $order['kv'],
        'deliv_date' => $order['date'],
        'deliv_time' => $order['time'],
        'pillows' => json_encode($order['lines'])
    ]);
    
    $response->write("ok");

});

$app->get('/ajax/orders', function ($request, $response, $args) {

    $db = $this->db;
    $orders = [];
    $db->Query(['orders', 'clients'])->where('client_id', 'clients.id')->sort('date', -1)->select('*', function($row) use (&$orders) {

        array_push($orders, [
            'id' => $row[0],
            'date' => $row['date'],
            'client_id' => $row['client_id'],
            'status' => $row['status'],
            'delivery' => $row['delivery'],
            'street' => $row['street'],
            'house' => $row['house'],
            'apartment' => $row['apartment'],
            'deliv_date' => $row['deliv_date'],
            'deliv_time' => $row['deliv_time'],
            'pillows' => $row['pillows'],
            'fio' => $row['fio'],
            'phone' => $row['phone'],
            'orders_count' => $row['orders_count'],
            'total_cost' => $row['total_cost'],
        ]);

    });

    $response->withJson($orders);

});
$app->put('/ajax/order/status', function ($request, $response, $args) {

    $order = $request->getParsedBody();

    $db = $this->db;
    $db->Query('orders')->where('id', $order['id'])->update([
        'status' => $order['status']
    ]);

    if($order['status'] == '3') {

        $data = $db->Query('orders')->where('id', $order['id'])->select_one();
        $client = $db->Query('clients')->where('id', $data['client_id'])->select_one();

        $db->Query('clients')->where('id', $data['client_id'])->update([
            'total_cost' => $client['total_cost'] + $this->tools['calc_sum']($data),
            'orders_count' =>  $client['orders_count'] + 1
        ]);

    }

    $response->withJson(['status' => 'ok']);

});
$app->put('/ajax/order/pillows', function ($request, $response, $args) {

    $order = $request->getParsedBody();

    $db = $this->db;
    $db->Query('orders')->where('id', $order['id'])->update([
        'pillows' => json_encode($order['lines'])
    ]);

    $response->withJson(['status' => 'ok']);

});
$app->put('/ajax/order/delivery', function ($request, $response, $args) {

    $order = $request->getParsedBody();

    $db = $this->db;
    $db->Query('orders')->where('id', $order['id'])->update([
        "apartment" => isset($order["apartment"]) ? $order["apartment"] : "",
        "comment" => isset($order["comment"]) ? $order["comment"] : "",
        "deliv_date" => $order["deliv_date"],
        "deliv_time" => $order["deliv_time"],
        "delivery" => $order["delivery"] ? 1 : 0,
        "house" => $order["house"],
        "street" => $order["street"]
    ]);

    $response->withJson(['status' => 'ok']);

});
$app->get('/ajax/clients', function ($request, $response, $args) {

    $db = $this->db;
    $client = $db->Query('clients')->sort('register_date', -1)->select('*');

    $response->withJson($client);

});
$app->put('/ajax/client/info', function ($request, $response, $args) {

    $client = $request->getParsedBody();

    $db = $this->db;
    $db->Query('clients')->where('id', $client['id'])->update([
        'fio' => $client['fio'],
        'phone' => $client['phone']
    ]);

    $response->withJson(['status' => 'ok']);

});
$app->get('/ajax/stat', function ($request, $response, $args) {

    $db = $this->db;
    $orders = $db->Query('orders')->select_one('count(*)');
    $clients = $db->Query('clients')->select_one('count(*)');
    $money = $db->Query('clients')->select_one('sum(total_cost)');
    $first_order = $db->Query('orders')->sort('id', 1)->select_one();
    $interval = time() - $first_order['date'];
    $days = round($interval / 60 / 60 / 24);
    if($days == 0)
        $days = 1;

    $stat = [
        'orders' => $orders,
        'clients' => $clients,
        'money' => $money,
        'money_in_day' => round($money / $days)
    ];

    
    $response->withJson($stat);

});