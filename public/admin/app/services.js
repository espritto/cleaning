
app.service('tools', function ($q) {
    var self = this;
    
    
    self.prices = [
        [300,250,250,230],
        [330,280,280,260],
        [370,320,320,300]
    ];

    self.statuses = {
        0: 'Новый',
        1: 'В работе',
        2: 'Ожидает оплаты',
        3: 'Завершен'
    };
    
    return self;
});
