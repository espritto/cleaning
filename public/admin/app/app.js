var app = angular.module('App', ['ngMaterial', 'ui.router']);
app.config(function($mdThemingProvider, $mdIconProvider, $interpolateProvider, $stateProvider, $urlRouterProvider){

      $interpolateProvider.startSymbol('[[').endSymbol(']]');

      $mdIconProvider
          .defaultIconSet("./admin/assets/svg/avatars.svg", 128)
          .icon("menu"       , "./admin/assets/svg/menu.svg"        , 24)
          .icon("share"      , "./admin/assets/svg/share.svg"       , 24)
          .icon("google_plus", "./admin/assets/svg/google_plus.svg" , 512)
          .icon("hangouts"   , "./admin/assets/svg/hangouts.svg"    , 512)
          .icon("twitter"    , "./admin/assets/svg/twitter.svg"     , 512)
          .icon("phone"      , "./admin/assets/svg/phone.svg"       , 512);

      $mdThemingProvider.theme('default')
          .primaryPalette('light-blue')
          .accentPalette('red');



      $urlRouterProvider.otherwise("/dashboard");

      $stateProvider
          .state('dashboard', {
                url: "/dashboard",
                templateUrl: "admin/app/view/dashboard.html",
                controller: "DashboardCtrl"
          })
          .state('orders', {
                url: "/orders",
                templateUrl: "admin/app/view/orders.html",
                controller: "OrdersCtrl"
          })
          .state('clients', {
                url: "/clients",
                templateUrl: "admin/app/view/clients.html",
                controller: "ClientsCtrl"
          });
      
      
});