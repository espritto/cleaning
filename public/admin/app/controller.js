
app.controller('AdminController', function ($scope, $mdBottomSheet, $timeout) {

    $scope.menuItems = [
        {url: 'dashboard', title: 'Главная', icon: ''},
        {url: 'orders', title: 'Заказы', icon: ''},
        {url: 'clients', title: 'Клиенты', icon: ''}
    ];
    
    
    

});
app.controller('DashboardCtrl', function ($scope, $http) {

    $scope.stat = {};

    $http.get('/ajax/stat').then(function (result) {
        $scope.stat = result.data;
        console.log('stat', result.data);
    });

    
});
app.controller('OrdersCtrl', function ($scope, $http, tools, $mdDialog, $mdToast) {

    $scope.orders = [];
    $scope.statusFilter = 0;
    $scope.statuses = _.map(tools.statuses, function(item, key){
        return {
            val: key,
            title: item
        }
    });

    $http.get('/ajax/orders').then(function (result) {
        $scope.orders = _.map(result.data, function(item){
            item.dateStr = moment(item.date * 1000).format('DD.MM.YY HH:mm');
            item.delivery = item.delivery == "1";
            item.lines = JSON.parse(item.pillows);
            return item;
        });
        console.log('orders', result.data);
    });

    var showToast = function () {
        $mdToast.show(
            $mdToast.simple()
                .textContent('Данные сохранены')
                .position('top right')
                .hideDelay(3000)
        );
    };
    
    var pillowsController = function ($scope, $mdDialog, order) {

        $scope.lines = _.clone(order.lines);

        $scope.sizes = [
            {val: 1, title: 'Маленькая'},
            {val: 2, title: 'Средняя'},
            {val: 3, title: 'Большая'}
        ];
        $scope.delivTypes = [
            {val: 1, title: 'Аэрочистка'},
            {val: 2, title: 'Сухая чистка (ультрафиолет)'},
            {val: 3, title: 'Паровая чистка'},
            {val: 4, title: 'Химчистка'}
        ];
        
        $scope.calcLineCost = function(line){
            return tools.prices[line.size-1][line.type-1];
        };
        
        $scope.add = function(){
            $scope.lines.push({
                size: 1,
                type: 1
            });
        };

        $scope.calcTotalCost = function(){
            var total = 0, min = 99999;
            var count = $scope.lines.length;
            $scope.lines.forEach(function(line){
                var sum = $scope.calcLineCost(line);
                if(sum < min)
                    min = sum;
                total += sum;
            });

            if(count >= 3) {
                total -= min;
                $scope.discount = min;
            }

            if(order.delivery) {
                $scope.delivDiscount = 0;
                if(total < 1000) {
                    total += 150;
                    $scope.delivDiscount = 150;
                }
            }

            return total;
        };

        $scope.remove = function(index){
            if($scope.lines.length == 1)
                return;
            $scope.lines.splice(index, 1);
        };

        $scope.close = function () {
            $mdDialog.cancel();
        };
        $scope.save = function () {
            $mdDialog.hide($scope.lines);
        }

    };

    var statusController = function ($scope, status, $mdDialog) {

        $scope.status = status;

        $scope.statuses = _.map(tools.statuses, function(item, key){
            return {
                val: key,
                title: item
            }
        });

        $scope.save = function () {
            $mdDialog.hide($scope.status);
        };
        $scope.close = function () {
            $mdDialog.cancel();
        };
    };

    var deliveryController = function ($scope, order, $mdDialog) {

        $scope.order = order;

        $scope.delivTimes = [
            {val: 1, title: 'с 9:00 до 12:00'},
            {val: 2, title: 'с 12:00 до 16:00'},
            {val: 3, title: 'с 16:00 до 19:00'},
            {val: 4, title: 'с 19:00 до 22:00'}
        ];
        $scope.delivDates = [
            {val: 1, title: 'Сегодня'},
            {val: 2, title: 'Завтра'},
            {val: 3, title: 'Послезавтра'}
        ];

        $scope.save = function (order) {
            $mdDialog.hide(order);
        };
        $scope.close = function () {
            $mdDialog.cancel();
        };
    };
    
    $scope.openPillows = function (order, ev) {

        $mdDialog.show({
                controller: pillowsController,
                templateUrl: 'admin/app/view/dialog_order_pillows.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: false,
                locals: {
                    order: order
                }
            })
            .then(function(lines) {
                order.lines = lines;
                $http.put('/ajax/order/pillows', {id: order.id, lines: lines}).then(function () {
                    showToast();
                });
                
            }, function() {

            });

    };

    $scope.openStatus = function (order, ev) {

        $mdDialog.show({
                controller: statusController,
                templateUrl: 'admin/app/view/dialog_order_status.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: false,
                locals: {
                    status: order.status
                }
            })
            .then(function(status) {
                order.status = status;
                $http.put('/ajax/order/status', {id: order.id, status: order.status}).then(function () {
                    showToast();
                });
            }, function() {

            });

    };

    $scope.openDelivery = function (order, ev) {

        $mdDialog.show({
                controller: deliveryController,
                templateUrl: 'admin/app/view/dialog_order_delivery.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: false,
                locals: {
                    order: order
                }
            })
            .then(function(newOrder) {

                $http.put('/ajax/order/delivery', newOrder).then(function () {
                    showToast();
                });

            }, function() {

            });

    };

    $scope.statusText = function (val) {
        return tools.statuses[val];
    };

});
app.controller('ClientsCtrl', function ($scope, $http, tools, $mdDialog, $mdToast) {

    $scope.clients = [];
    $http.get('/ajax/clients').then(function (result) {
        $scope.clients = _.map(result.data, function(item){
            item.dateStr = moment(item.register_date * 1000).format('DD.MM.YY HH:mm');
            return item;
        });
        console.log('clients', result.data);
    });

    var clientEditController = function ($scope, client) {

        $scope.client = client;

        $scope.save = function () {
            $mdDialog.hide($scope.client);
        };
        $scope.close = function () {
            $mdDialog.cancel();
        };

    };

    var showToast = function () {
        $mdToast.show(
            $mdToast.simple()
                .textContent('Данные сохранены')
                .position('top right')
                .hideDelay(3000)
        );
    };

    $scope.openEdit = function (client, ev) {

        $mdDialog.show({
                controller: clientEditController,
                templateUrl: 'admin/app/view/dialog_client.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: false,
                locals: {
                    client: {fio: client.fio, phone: client.phone}
                }
            })
            .then(function(clientDate) {

                clientDate.id = client.id;
                client.fio = clientDate.fio;
                client.phone = clientDate.phone;

                $http.put('/ajax/client/info', clientDate).then(function () {
                    showToast();
                });

            }, function() {

            });

    };

});
