angular.module('Order', []).
config(function($interpolateProvider){
    $interpolateProvider.startSymbol('[[').endSymbol(']]');
}).
controller('MainCtrl', function ($scope, $http) {

    $scope.lines = [{
        size: 1,
        type: 1
    }];
    $scope.order = {
        time: 1,
        date: 1,
        isDelivery: false
    };

    var prices = [
        [300,250,250,230],
        [330,280,280,260],
        [370,320,320,300]
    ];

    $scope.sizes = [
        {val: 1, title: 'Маленькая'},
        {val: 2, title: 'Средняя'},
        {val: 3, title: 'Большая'}
    ];
    $scope.delivTypes = [
        {val: 1, title: 'Аэрочистка'},
        {val: 2, title: 'Сухая чистка (ультрафиолет)'},
        {val: 3, title: 'Паровая чистка'},
        {val: 4, title: 'Химчистка'}
    ];
    $scope.delivTimes = [
        {val: 1, title: 'с 9:00 до 12:00'},
        {val: 2, title: 'с 12:00 до 16:00'},
        {val: 3, title: 'с 16:00 до 19:00'},
        {val: 4, title: 'с 19:00 до 22:00'}
    ];
    $scope.delivDates = [
        {val: 1, title: 'Сегодня'},
        {val: 2, title: 'Завтра'},
        {val: 3, title: 'Послезавтра'}
    ];


    $scope.add = function(){
        $scope.lines.push({
            size: 1,
            type: 1
        });
    };
    $scope.doOrder = function(){

        var orderData = _.clone($scope.order);
        orderData.lines = $scope.lines;

        if(!orderData.fio)
            return alert('Укажите свое имя в заказе');

        if(!orderData.phone)
            return alert('Обязательно укажите номер своего телефона');

        if($scope.order.isDelivery) {
            if(!orderData.street)
                return alert('Укажите улицу доставки');
            if(!orderData.house)
                return alert('Укажите дом доставки');
        }

        orderData.phone = orderData.phone.substr(-10);

        $http.post('/ajax/order', orderData).then(function(data){
            
            console.log('order', data);

            alert('Ваш заказ принят, дождитесь звонка оператора для уточнения заказа');
            
        });
        
    };

    $scope.remove = function(index){
        if($scope.lines.length == 1)
            return;
        $scope.lines.splice(index, 1);
    };
    $scope.calcLineCost = function(line){
        return prices[line.size-1][line.type-1];
    };
    $scope.calcTotalCost = function(){
        var total = 0, min = 99999;
        var count = $scope.lines.length;
        $scope.lines.forEach(function(line){
            var sum = $scope.calcLineCost(line);
            if(sum < min)
                min = sum;
            total += sum;
        });

        if(count >= 3) {
            total -= min;
            $scope.discount = min;
        }
        
        if($scope.order.isDelivery) {
            $scope.delivDiscount = 0;
            if(total < 1000) {
                total += 150;
                $scope.delivDiscount = 150;
            }
        }

        return total;
    };


    /* JQUERY */

    $('input[name="delivery"]').click(function () {
        var state = $(this).val();
        if(state == 0) {
            $scope.order.isDelivery = false;
            $('.address').slideUp();
        } else {
            $scope.order.isDelivery = true;
            $('.address').slideDown();
        }
        $scope.$apply();

    });

});